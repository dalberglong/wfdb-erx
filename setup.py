from setuptools import setup, find_packages


setup(
    name='wfdb_lite',

    version="0.0.1",

    description='The wfdb_erx Python Toolbox',

    license='MIT',

    keywords='wfdb_erx clinical waveform',

    packages=find_packages(),
)
