import re

import numpy as np


int_types = (int, np.int64, np.int32, np.int16, np.int8)
float_types = (float, np.float64, np.float32) + int_types

_SPECIFICATION_COLUMNS = [
    'allowed_types',
    'delimiter',
    'dependency',
    'write_required',
    'read_default',
    'write_default'
]

RECORD_SPECS = {
    'record_name': {
        'allowed_types': (str,),
        'delimiter': '',
        'dependency': None,
        'write_required': True,
        'read_default': None,
        'write_default': None
    },
    'n_seg': {
        'allowed_types': int_types,
        'delimiter': '/',
        'dependency': 'record_name',
        'write_required': True,
        'read_default': None,
        'write_default': None
    },
    'n_sig': {
        'allowed_types': int_types,
        'delimiter': ' ',
        'dependency': 'record_name',
        'write_required': True,
        'read_default': None,
        'write_default': None
    },
    'fs': {
        'allowed_types': float_types,
        'delimiter': ' ',
        'dependency': 'n_sig',
        'write_required': True,
        'read_default': 250,
        'write_default': None
    },
    'counter_freq': {
        'allowed_types': float_types,
        'delimiter': '/',
        'dependency': 'fs',
        'write_required': False,
        'read_default': None,
        'write_default': None
    },
    'base_counter': {
        'allowed_types': float_types,
        'delimiter': '(',
        'dependency': 'counter_freq',
        'write_required': False,
        'read_default': None,
        'write_default': None
    },
    'sig_len': {
        'allowed_types': int_types,
        'delimiter': ' ',
        'dependency': 'fs',
        'write_required': True,
        'read_default': None,
        'write_default': None
    }
}

SIGNAL_SPECS = {
    'file_name': {
        'allowed_types': (str,),
        'delimiter': '',
        'dependency': None,
        'write_required': True,
        'read_default': None,
        'write_default': None
    },
    'fmt': {
        'allowed_types': (str,),
        'delimiter': ' ',
        'dependency': 'file_name',
        'write_required': True,
        'read_default': None,
        'write_default': None
    },
    'samps_per_frame': {
        'allowed_types': int_types,
        'delimiter': 'x',
        'dependency': 'fmt',
        'write_required': False,
        'read_default': 1,
        'write_default': None
    },
    'skew': {
        'allowed_types': int_types,
        'delimiter': ':',
        'dependency': 'fmt',
        'write_required': False,
        'read_default': None,
        'write_default': None
    },
    'byte_offset': {
        'allowed_types': int_types,
        'delimiter': '+',
        'dependency': 'fmt',
        'write_required': False,
        'read_default': None,
        'write_default': None
    },
    'adc_gain': {
        'allowed_types': float_types,
        'delimiter': ' ',
        'dependency': 'fmt',
        'write_required': True,
        'read_default': 200.,
        'write_default': None
    },
    'baseline': {
        'allowed_types': int_types,
        'delimiter': '(',
        'dependency': 'adc_gain',
        'write_required': True,
        'read_default': 0,
        'write_default': None
    },
    'units': {
        'allowed_types': (str,),
        'delimiter': '/',
        'dependency': 'adc_gain',
        'write_required': True,
        'read_default': 'mV',
        'write_default': None
    },
    'adc_res': {
        'allowed_types': int_types,
        'delimiter': ' ',
        'dependency': 'adc_gain',
        'write_required': False,
        'read_default': None,
        'write_default': 0
    },
    'adc_zero': {
        'allowed_types': int_types,
        'delimiter': ' ',
        'dependency': 'adc_res',
        'write_required': False,
        'read_default': None,
        'write_default': 0
    },
    'init_value': {
        'allowed_types': int_types,
        'delimiter': ' ',
        'dependency': 'adc_zero',
        'write_required': False,
        'read_default': None,
        'write_default': None
    },
    'checksum': {
        'allowed_types': int_types,
        'delimiter': ' ',
        'dependency': 'init_value',
        'write_required': False,
        'read_default': None,
        'write_default': None
    },
    'block_size': {
        'allowed_types': int_types,
        'delimiter': ' ',
        'dependency': 'checksum',
        'write_required': False,
        'read_default': None,
        'write_default': 0
    },
    'sig_name': {
        'allowed_types': (str,),
        'delimiter': ' ',
        'dependency': 'block_size',
        'write_required': False,
        'read_default': None,
        'write_default': None
    },
}

# The bit resolution of each wfdb_erx dat format
BIT_RES = {'8': 8, '16': 16, '24': 24, '32': 32, '61': 16, '80': 8,
           '160': 16, '212': 12, '310': 10, '311': 10}

# Record line
_rx_record = re.compile(''.join(
    ["(?P<record_name>[-\w]+)/?(?P<n_seg>\d*)[ \t]+",
     "(?P<n_sig>\d+)[ \t]*(?P<fs>\d*\.?\d*)/*(?P<counter_freq>-?\d*\.?\d*)",
     "\(?(?P<base_counter>-?\d*\.?\d*)\)?[ \t]*(?P<sig_len>\d*)[ \t]*"])
)

# Signal line
_rx_signal = re.compile(''.join(
    ["(?P<file_name>~?[-\w]*\.?[\w]*)[ \t]+(?P<fmt>\d+)x?"
     "(?P<samps_per_frame>\d*):?(?P<skew>\d*)\+?(?P<byte_offset>\d*)[ \t]*",
     "(?P<adc_gain>-?\d*\.?\d*e?[\+-]?\d*)\(?(?P<baseline>-?\d*)\)?",
     "/?(?P<units>[\w\^\-\?%\/]*)[ \t]*(?P<adc_res>\d*)[ \t]*",
     "(?P<adc_zero>-?\d*)[ \t]*(?P<init_value>-?\d*)[ \t]*(?P<checksum>-?\d*)",
     "[ \t]*(?P<block_size>\d*)[ \t]*(?P<sig_name>[\S]?[^\t\n\r\f\v]*)"])
)

# Numpy dtypes used to load dat files of each format.
DATA_LOAD_TYPES = {'8': '<i1', '16': '<i2', '24': '<i3', '32': '<i4',
                   '61': '>i2', '80': '<u1', '160': '<u2', '212': '<u1',
                   '310': '<u1', '311': '<u1'}


# Bytes required to hold each sample (including wasted space) for each
# wfdb_erx dat formats
BYTES_PER_SAMPLE = {'8': 1, '16': 2, '24': 3, '32': 4, '61': 2, '80': 1,
                    '160': 2, '212': 1.5, '310': 4 / 3., '311': 4 / 3.}

# Formats in which not all samples align with byte boundaries
UNALIGNED_FMTS = ['212', '310', '311']

# Formats which are stored in offset binary form
OFFSET_FMTS = ['80', '160']
