import re

import s3fs
import numpy as np

from wfdb_lite.utils import describe_list_indices, _np_dtype, _fmt_res,\
    _dat_read_params, _required_byte_num, _digi_nan, _blocks_to_samples, _skew_sig, _check_sig_dims
from wfdb_lite.config import RECORD_SPECS, SIGNAL_SPECS, BYTES_PER_SAMPLE, DATA_LOAD_TYPES,\
    UNALIGNED_FMTS, OFFSET_FMTS, int_types, float_types, _rx_record, _rx_signal


def get_header(subject_id=None, dataset_id=None):
    if subject_id is not None and dataset_id is not None:
        fs = s3fs.S3FileSystem()

        url = f'erx-mimic/{subject_id}/{dataset_id}.hea'

        with fs.open(url) as f:
            header = f.read()

        filelines = header.decode('iso-8859-1').splitlines()

        header_lines = []
        comment_lines = []
        for line in filelines:
            line = str(line.strip())
            # Comment line
            if line.startswith('#'):
                comment_lines.append(line)
            # Non-empty non-comment line = header line.
            elif line:
                # Look for a comment in the line
                ci = line.find('#')
                if ci > 0:
                    header_lines.append(line[:ci])
                    # comment on same line as header line
                    comment_lines.append(line[ci:])
                else:
                    header_lines.append(line)

        # Dictionary for record fields
        record_fields = {}

        # Read string fields from record line
        (
            record_fields['record_name'], record_fields['n_seg'],
            record_fields['n_sig'], record_fields['fs'],
            record_fields['counter_freq'], record_fields['base_counter'],
            record_fields['sig_len']
        ) = re.findall(_rx_record, header_lines[0])[0]

        for field in RECORD_SPECS.keys():
            # Replace empty strings with their read defaults (which are
            # mostly None)
            if record_fields[field] == '':
                record_fields[field] = RECORD_SPECS[field]['read_default']
            # Typecast non-empty strings for non-string (numerical/datetime)
            # fields
            else:
                if RECORD_SPECS[field]['allowed_types'] == int_types:
                    record_fields[field] = int(record_fields[field])
                elif RECORD_SPECS[field]['allowed_types'] == float_types:
                    record_fields[field] = float(record_fields[field])
                    # cast fs to an int if it is close
                    if field == 'fs':
                        fs = float(record_fields['fs'])
                        if round(fs, 8) == float(int(fs)):
                            fs = int(fs)
                        record_fields['fs'] = fs

        return record_fields, header_lines
    else:
        return None, None


def parse_sigs(signal_lines):
    n_sig = len(signal_lines)
    # Dictionary for signal fields
    signal_fields = {}

    # Each dictionary field is a list
    for field in SIGNAL_SPECS.keys():
        signal_fields[field] = n_sig * [None]

    # Read string fields from signal line
    for ch in range(n_sig):
        (signal_fields['file_name'][ch], signal_fields['fmt'][ch],
         signal_fields['samps_per_frame'][ch], signal_fields['skew'][ch],
         signal_fields['byte_offset'][ch], signal_fields['adc_gain'][ch],
         signal_fields['baseline'][ch], signal_fields['units'][ch],
         signal_fields['adc_res'][ch], signal_fields['adc_zero'][ch],
         signal_fields['init_value'][ch], signal_fields['checksum'][ch],
         signal_fields['block_size'][ch],
         signal_fields['sig_name'][ch]) = _rx_signal.findall(signal_lines[ch])[0]

        for field in SIGNAL_SPECS.keys():
            # Replace empty strings with their read defaults (which are mostly None)
            # Note: Never set a field to None. [None]* n_sig is accurate, indicating
            # that different channels can be present or missing.
            if signal_fields[field][ch] == '':
                signal_fields[field][ch] = SIGNAL_SPECS[field]['read_default']

                # Special case: missing baseline defaults to ADCzero if present
                if field == 'baseline' and signal_fields['adc_zero'][ch] != '':
                    signal_fields['baseline'][ch] = int(signal_fields['adc_zero'][ch])
            # Typecast non-empty strings for numerical fields
            else:
                if SIGNAL_SPECS[field]['allowed_types'] is int_types:
                    signal_fields[field][ch] = int(signal_fields[field][ch])
                elif SIGNAL_SPECS[field]['allowed_types'] is float_types:
                    signal_fields[field][ch] = float(signal_fields[field][ch])
                    # Special case: adc_gain of 0 means 200
                    if field == 'adc_gain' and signal_fields['adc_gain'][ch] == 0:
                        signal_fields['adc_gain'][ch] = 200.

    return signal_fields


def get_signals(subject_id=None, dataset_id=None, header=None, header_lines=None, smooth_frames=True):

    if subject_id is not None and dataset_id is not None and header is not None and header_lines is not None:
        # Single segment header - Process signal specification lines
        if header['n_seg'] is not None:
            raise Exception("Number of segments is not None, requires MultiRecord processing")

        record = {
            'p_signal': None, 'd_signal': None, 'e_p_signal': None,
            'e_d_signal': None, 'record_name': None, 'n_sig': None,
            'fs': None, 'counter_freq': None, 'base_counter': None,
            'sig_len': None, 'base_time': None, 'base_date': None,
            'file_name': None, 'fmt': None, 'samps_per_frame': None,
            'skew': None, 'byte_offset': None, 'adc_gain': None,
            'baseline': None, 'units': None, 'adc_res': None,
            'adc_zero': None, 'init_value': None, 'checksum': None,
            'block_size': None, 'sig_name': None, 'comments': None
        }

        # There are signals
        if len(header_lines) > 1:
            # Read the fields from the signal lines
            signal_fields = parse_sigs(header_lines[1:])

            # Set the object's signal fields
            for key in signal_fields.keys():
                if key == 'n_seg':
                    continue
                record[key] = signal_fields[key]

        # Set the object's record line fields
        for key in header.keys():
            if key == 'n_seg':
                continue
            record[key] = header[key]

        sampfrom = 0
        sampto = record['sig_len']

        # Get Signals #

        # Set defaults for empty fields
        for i in range(record['n_sig']):
            if record['byte_offset'][i] is None:
                record['byte_offset'][i] = 0
            if record['samps_per_frame'][i] is None:
                record['samps_per_frame'][i] = 1
            if record['skew'][i] is None:
                record['skew'][i] = 0

        file_name, datchannel = describe_list_indices(record['file_name'])

        # Some files will not be read depending on input channels.
        # Get the the wanted fields only.
        w_file_name = []  # one scalar per dat file
        w_fmt = {}  # one scalar per dat file
        w_byte_offset = {}  # one scalar per dat file
        w_samps_per_frame = {}  # one list per dat file
        w_skew = {}  # one list per dat file
        w_channel = {}  # one list per dat file

        channels = list(range(record['n_sig']))

        for fn in file_name:
            # intersecting dat channels between the input channels and the channels of the file
            idc = [c for c in datchannel[fn] if c in channels]

            # There is at least one wanted channel in the dat file
            if idc != []:
                w_file_name.append(fn)
                w_fmt[fn] = record['fmt'][datchannel[fn][0]]
                w_byte_offset[fn] = record['byte_offset'][datchannel[fn][0]]
                w_samps_per_frame[fn] = [record['samps_per_frame'][c] for c in datchannel[fn]]
                w_skew[fn] = [record['skew'][c] for c in datchannel[fn]]
                w_channel[fn] = idc

        # Wanted dat channels, relative to the dat file itself
        r_w_channel = {}
        # The channels in the final output array that correspond to the read channels in each dat file
        out_dat_channel = {}
        for fn in w_channel:
            r_w_channel[fn] = [c - min(datchannel[fn]) for c in w_channel[fn]]
            out_dat_channel[fn] = [channels.index(c) for c in w_channel[fn]]

        # Figure out the largest required dtype for the segment to minimize memory usage
        max_dtype = _np_dtype(_fmt_res(record['fmt'], max_res=True), discrete=True)
        # Allocate signal array. Minimize dtype
        signals = np.zeros([sampto - sampfrom, len(channels)], dtype=max_dtype)

        # Read each wanted dat file and store signals
        for fn in w_file_name:
            # Total number of samples per frame
            tsamps_per_frame = sum(record['samps_per_frame'])
            # The signal length to read (per channel)
            read_len = sampto - sampfrom

            # Calculate parameters used to read and process the dat file
            (
                start_byte, n_read_samples, block_floor_samples, extra_flat_samples, nan_replace
            ) = _dat_read_params(
                w_fmt[fn], record['sig_len'], w_byte_offset[fn], w_skew[fn], tsamps_per_frame, sampfrom, sampto
            )

            # Number of bytes to be read from the dat file
            total_read_bytes = _required_byte_num('read', w_fmt[fn], n_read_samples)

            # Total samples to be processed in intermediate step. Includes extra
            # padded samples beyond dat file
            total_process_samples = n_read_samples + extra_flat_samples

            # Total number of bytes to be processed in intermediate step.
            total_process_bytes = _required_byte_num('read', w_fmt[fn], total_process_samples)

            # element_count is the number of elements to read using np.fromfile
            # for local files
            # byte_count is the number of bytes to read for streaming files
            if w_fmt[fn] == '212':
                byte_count = _required_byte_num('read', '212', n_read_samples)
                element_count = byte_count
            elif w_fmt[fn] in ['310', '311']:
                byte_count = _required_byte_num('read', w_fmt[fn], n_read_samples)
                element_count = byte_count
            else:
                element_count = n_read_samples
                byte_count = n_read_samples * BYTES_PER_SAMPLE[w_fmt[fn]]

            if DATA_LOAD_TYPES[w_fmt[fn]] == '<i3':
                dtype_in = '<i3'
            else:
                dtype_in = np.dtype(DATA_LOAD_TYPES[w_fmt[fn]])

            fs = s3fs.S3FileSystem()

            with fs.open(f'erx-mimic/{subject_id}/{dataset_id}.dat', 'rb') as f:
                content = f.read()

            end_byte = start_byte + byte_count
            content = content[start_byte:end_byte]

            data_to_read = np.frombuffer(content, dtype=dtype_in).T

            if extra_flat_samples:
                if w_fmt[fn] in UNALIGNED_FMTS:
                    # Extra number of bytes to append onto the bytes read from
                    # the dat file.
                    n_extra_bytes = total_process_bytes - total_read_bytes

                    sig_data = np.concatenate((
                        data_to_read, np.zeros(n_extra_bytes, dtype=np.dtype(DATA_LOAD_TYPES[w_fmt[fn]]))
                    ))
                else:
                    sig_data = np.concatenate((
                        data_to_read, np.zeros(extra_flat_samples, dtype=np.dtype(DATA_LOAD_TYPES[w_fmt[fn]]))
                    ))
            else:
                sig_data = data_to_read

            # For unaligned fmts, turn the uint8 blocks into actual samples
            if w_fmt[fn] in UNALIGNED_FMTS:
                sig_data = _blocks_to_samples(sig_data, total_process_samples, w_fmt[fn])
                # Remove extra leading sample read within the byte block if any
                if block_floor_samples:
                    sig_data = sig_data[block_floor_samples:]

            # Adjust samples values for byte offset formats
            if w_fmt[fn] in OFFSET_FMTS:
                if w_fmt[fn] == '80':
                    sig_data = (sig_data.astype('int16') - 128).astype('int8')
                elif w_fmt[fn] == '160':
                    sig_data = (sig_data.astype('int32') - 32768).astype('int16')

            # No extra samples/frame. Obtain original uniform numpy array
            if tsamps_per_frame == record['n_sig']:
                # Reshape into multiple channels
                signal = sig_data.reshape(-1, record['n_sig'])
                # Skew the signal
                signal = _skew_sig(signal, w_skew[fn], record['n_sig'], read_len, w_fmt[fn], nan_replace)
            # Extra frames present to be smoothed. Obtain averaged uniform numpy array
            # Allocate memory for smoothed signal.
            signal = np.zeros((int(len(sig_data) / tsamps_per_frame), record['n_sig']), dtype=sig_data.dtype)

            # Transfer and average samples
            for ch in range(record['n_sig']):
                if record['samps_per_frame'][ch] == 1:
                    signal[:, ch] = sig_data[sum(([0] + record['samps_per_frame'])[:ch + 1])::tsamps_per_frame]
                else:
                    if ch == 0:
                        startind = 0
                    else:
                        startind = np.sum(record['samps_per_frame'][:ch])
                    signal[:, ch] = [np.average(sig_data[ind:ind + record['samps_per_frame'][ch]]) for ind in range(startind, len(sig_data), tsamps_per_frame)]
            # Skew the signal
            signal = _skew_sig(signal, w_skew[fn], record['n_sig'], read_len, w_fmt[fn], nan_replace)

            # Integrity check of signal shape after reading
            _check_sig_dims(signal, read_len, record['n_sig'], record['samps_per_frame'])

            signals[:, out_dat_channel[fn]] = signal[:, r_w_channel[fn]]

            # Digital Analog Conversion

            # The digital NAN values for each channel
            d_nans = _digi_nan(record['fmt'])
            floatdtype = 'float32'

            p_signal = np.zeros(signals.shape)
            for ch in range(record['n_sig']):
                # NAN locations for the channel
                ch_nanlocs = signals[:, ch] == d_nans[ch]
                ch_p_signal = signals[:, ch].astype(floatdtype, copy=False)
                np.subtract(ch_p_signal, record['baseline'][ch], ch_p_signal)
                np.divide(ch_p_signal, record['adc_gain'][ch], ch_p_signal)
                ch_p_signal[ch_nanlocs] = np.nan
                p_signal[:, ch] = ch_p_signal

    else:
        record['sig_name'] = []
        p_signal = []

    return p_signal, record['sig_name']


if __name__ == "__main__":
    subject_id = 'p000188'
    dataset_id = '3285727_0014'

    header, header_lines = get_header(subject_id=subject_id, dataset_id=dataset_id)

    signals, sig_names = get_signals(subject_id=subject_id, dataset_id=dataset_id, header=header, header_lines=header_lines)

    print(header)
    print(header_lines)
    print(signals)
    print(sig_names)
